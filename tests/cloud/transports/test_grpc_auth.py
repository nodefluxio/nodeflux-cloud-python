# Copyright 2018 PT Nodeflux Teknologi Indonesia.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""
    tests.cloud.transports.grpc_auth
    ~~~~~~~~~~~~~~~~~~~~~~~

    Test cases for the nodeflux.cloud.transports.grpc_auth module.
"""

import pytest
from unittest.mock import MagicMock, create_autospec

from nodeflux.cloud.auth.credentials import Credentials
from nodeflux.cloud.transports.grpc_auth import AuthMetadataPlugin
from nodeflux.cloud.auth.signer import AuthSignerV1, Request

mocked_class_context = MagicMock()
mocked_class_context.service_url = 'http://example.com'
mocked_class_context.method_name = 'testing'


def callback(item, data):
    pass


mocked_callback = create_autospec(callback)


class TestAuthMetadataPlugin:
    """Test AuthMetadataPlugin to works well"""
    def test_get_authorization_headers(self, mocker):
        """Test method calls from meta data plugin can return value not None"""
        credentials = Credentials("SOMEAPIKEY", "SOMESECRETKEY")
        auth_meta_data_plugin = AuthMetadataPlugin(credentials)
        assert auth_meta_data_plugin._get_authorization_headers(
            mocked_class_context) is not None

    def test_method_call(self, mocker):
        mocker.spy(AuthMetadataPlugin, '__call__')
        credentials = Credentials("SOMEAPIKEY", "SOMESECRETKEY")
        metadata_plugin = AuthMetadataPlugin(credentials)
        metadata_plugin_call_result = metadata_plugin(mocked_class_context,
                                                      callback)

        assert AuthMetadataPlugin.__call__.call_count == 1
        assert metadata_plugin_call_result is None
