# Copyright 2018 PT Nodeflux Teknologi Indonesia.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""
    tests.cloud.transports.image_analytic_grpc_transport
    ~~~~~~~~~~~~~~~~~~~~~~~

    Test cases for the nodeflux.cloud.transports.image_analytic_grpc_transport module.
"""
import types
import os
import pytest

import grpc

from nodeflux.cloud.utils import environment_vars as env
from google.rpc.code_pb2 import Code
from nodeflux.cloud.transports import ImageAnalyticGrpcTransport
from nodeflux.cloud.requests.image_analytic_request import ImageAnalyticRequest
from nodeflux.cloud.requests.types import AnalyticTypes
from nodeflux.cloud.auth.credentials import Credentials
from nodeflux.api.v1beta1.image_analytic_pb2_grpc import ImageAnalyticStub
from nodeflux.api.v1beta1 import image_analytic_pb2


@pytest.fixture
def image_representation():
    """example of image read result"""
    return bytes([0x13, 0x00, 0x00, 0x00, 0x08, 0x00])


@pytest.fixture
def type_of_analytic_requested():
    """example of case type request"""
    return [
        AnalyticTypes.FACE_DETECTION, AnalyticTypes.FACE_DEMOGRAPHY,
        AnalyticTypes.FACE_RECOGNITION
    ]


@pytest.fixture
def request_object(image_representation, type_of_analytic_requested):
    return [
        ImageAnalyticRequest(
            image_representation,
            type_of_analytic_requested,
        )
    ]


@pytest.fixture
def response_object():
    return [image_analytic_pb2.ImageAnalyticResponse()]


class TestImageAnalyticGrpcTransport:
    """Test ImageAnalyticGrpcTransport that it works well"""
    def test_create_instance(self):
        """Test case for image analytics grpc transport creating instance"""

        os.environ[env.NODEFLUX_ACCESS_KEY] = "APIKEYFROMENV"
        os.environ[env.NODEFLUX_SECRET_KEY] = "SECRETKEYFROMENV"

        transports_instance = ImageAnalyticGrpcTransport()
        assert isinstance(transports_instance, ImageAnalyticGrpcTransport)
        assert isinstance(transports_instance._channel, grpc.Channel)
        assert isinstance(transports_instance._stub, ImageAnalyticStub)

    def test_create_instance_error_case(self):
        """Test case create instance error result because the credentials and channels create separately"""

        credentials = "a"
        channels = "b"
        with pytest.raises(ValueError):
            transports_instance = ImageAnalyticGrpcTransport(
                credentials, channels)

    def test_create_instance_provided_credentials(self):
        """Test case create instance error result because the credentials and channels create separately"""

        os.environ[env.NODEFLUX_ACCESS_KEY] = "APIKEYFROMENV"
        os.environ[env.NODEFLUX_SECRET_KEY] = "SECRETKEYFROMENV"

        credentials = Credentials()

        transports_instance = ImageAnalyticGrpcTransport(
            credentials=credentials)

        assert isinstance(transports_instance, ImageAnalyticGrpcTransport)

    def test_stream_image_analytic_success_sending_all_request(
            self, request_object, response_object, mocker):
        """Test case for image analytics grpc transport to work well when request of stream image analytic comes"""
        mocker.patch(
            'nodeflux.api.v1beta1.image_analytic_pb2_grpc.ImageAnalyticStub')

        channel_insecure = grpc.insecure_channel("localhost:50053")

        os.environ[env.NODEFLUX_ACCESS_KEY] = "APIKEYFROMENV"
        os.environ[env.NODEFLUX_SECRET_KEY] = "SECRETKEYFROMENV"

        transports_instance = ImageAnalyticGrpcTransport(
            channel=channel_insecure)

        mocker.patch.object(transports_instance._stub,
                            'StreamImageAnalytic',
                            return_value=response_object,
                            autospec=True)

        retval = transports_instance.stream_image_analytic(request_object)

        for value in retval:
            print("item is not empty")
            assert isinstance(value, image_analytic_pb2.ImageAnalyticResponse)

    def test_stream_image_analytic_return_error_if_not_error_unavaliable(
            self, request_object, mocker):
        """Test case for image analytics grpc transport to work well when request of stream image analytic comes"""
        mocker.patch(
            'nodeflux.api.v1beta1.image_analytic_pb2_grpc.ImageAnalyticStub')

        os.environ[env.NODEFLUX_ACCESS_KEY] = "APIKEYFROMENV"
        os.environ[env.NODEFLUX_SECRET_KEY] = "SECRETKEYFROMENV"

        transports_instance = ImageAnalyticGrpcTransport()

        retval = transports_instance.stream_image_analytic(request_object)

        with pytest.raises(grpc.RpcError):
            for value in retval:
                assert False, "value is not raise grpcerror"
