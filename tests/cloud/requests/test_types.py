# Copyright 2018 PT Nodeflux Teknologi Indonesia.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""
    tests.cloud.requests.types
    ~~~~~~~~~~~~~~~~~~~~~~~

    Test cases for the nodeflux.cloud.requests.types module.
"""

import pytest

from nodeflux.cloud.requests.types import AnalyticTypes
from nodeflux.analytics.v1beta1.analytic_pb2 import Analytic


class TestTypesEnumerator:
    """Unit test to ensure the output of enumerator is match. The number of
       test case should scale with the number of types avaliable in module"""
    def test_output_type_extension(self):
        return_type = AnalyticTypes.EXTENSION
        assert return_type == Analytic.EXTENSION

    def test_output_type_face_detection(self):
        return_type = AnalyticTypes.FACE_DETECTION
        assert return_type == Analytic.FACE_DETECTION

    def test_output_type_face_demography(self):
        return_type = AnalyticTypes.FACE_DEMOGRAPHY
        assert return_type == Analytic.FACE_DEMOGRAPHY

    def test_output_type_face_enrollment(self):
        return_type = AnalyticTypes.FACE_ENROLLMENT
        assert return_type == Analytic.FACE_ENROLLMENT

    def test_output_type_face_recognition(self):
        return_type = AnalyticTypes.FACE_RECOGNITION
        assert return_type == Analytic.FACE_RECOGNITION

    def test_output_type_vehicle_detection(self):
        return_type = AnalyticTypes.VEHICLE_DETECTION
        assert return_type == Analytic.VEHICLE_DETECTION

    def test_output_type_license_plate_recognition(self):
        return_type = AnalyticTypes.LICENSE_PLATE_RECOGNITION
        assert return_type == Analytic.LICENSE_PLATE_RECOGNITION
