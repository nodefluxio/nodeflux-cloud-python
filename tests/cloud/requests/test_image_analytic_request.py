# Copyright 2018 PT Nodeflux Teknologi Indonesia.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""
    tests.cloud.requests.image_analytic_request
    ~~~~~~~~~~~~~~~~~~~~~~~

    Test cases for the nodeflux.cloud.requests.image_analytic_request module.
"""

import pytest

from nodeflux.cloud.requests.image_analytic_request import ImageAnalyticRequest
from nodeflux.cloud.requests.types import AnalyticTypes


@pytest.fixture
def image_representation():
    """example of image read result"""
    return bytes([0x13, 0x00, 0x00, 0x00, 0x08, 0x00])


@pytest.fixture
def type_of_analytic_requested():
    """example of case type request"""
    return [
        AnalyticTypes.FACE_DETECTION, AnalyticTypes.FACE_DEMOGRAPHY,
        AnalyticTypes.FACE_RECOGNITION
    ]


@pytest.fixture
def labels():
    """example of case with labels"""
    return {"test": "success"}


@pytest.fixture
def request_object(image_representation, type_of_analytic_requested):
    return ImageAnalyticRequest(
        image_representation,
        type_of_analytic_requested,
    )


@pytest.fixture
def request_object2(image_representation, type_of_analytic_requested, labels):
    return ImageAnalyticRequest(image_representation,
                                type_of_analytic_requested, labels)


class TestImageAnalyticRequestCloudRequest:
    """Unit test to ensure the class of ImageAnalyticRequest can create right object"""
    def test_create_one_object_that_reflect_image_analytic_request(
            self, request_object, image_representation,
            type_of_analytic_requested):
        request = request_object

        assert request.image == image_representation
        assert request.analytics == type_of_analytic_requested
        assert request.labels is None
        assert request.timestamp is not None

    def test_create_one_object_that_reflect_image_analytic_request_with_labels(
            self, request_object2, image_representation,
            type_of_analytic_requested, labels):
        request = request_object2

        assert request.image == image_representation
        assert request.analytics == type_of_analytic_requested
        assert request.labels == labels
        assert request.timestamp is not None
