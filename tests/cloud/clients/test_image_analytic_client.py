# Copyright 2018 PT Nodeflux Teknologi Indonesia.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""
    tests.cloud.clients.image_analytic_client
    ~~~~~~~~~~~~~~~~~~~~~~~

    Test cases for the nodeflux.cloud.clients.image_analytic_client module.
"""
import types
import os
import pytest

import nodeflux.cloud.clients.image_analytic_client
from nodeflux.cloud.utils import environment_vars as env
from nodeflux.cloud.clients import ImageAnalyticClient
from nodeflux.cloud.requests import ImageAnalyticRequest, AnalyticTypes
from nodeflux.cloud.transports import ImageAnalyticGrpcTransport
from nodeflux.api.v1beta1 import image_analytic_pb2
from nodeflux.analytics.v1beta1 import analytic_pb2
from nodeflux.analytics.v1beta1 import face_enrollment_pb2


@pytest.fixture
def image_representation():
    """example of image read result"""
    return bytes([0x13, 0x00, 0x00, 0x00, 0x08, 0x00])


@pytest.fixture
def type_of_analytic_requested():
    """example of case type request"""
    return [
        AnalyticTypes.FACE_DETECTION, AnalyticTypes.FACE_DEMOGRAPHY,
        AnalyticTypes.FACE_RECOGNITION
    ]


@pytest.fixture
def request_object(image_representation, type_of_analytic_requested):
    return [
        ImageAnalyticRequest(
            image_representation,
            type_of_analytic_requested,
        )
    ]


@pytest.fixture
def response_object():
    fc_enrl_res = face_enrollment_pb2.FaceEnrollment(face_id=12333923)
    img_anly_resp = image_analytic_pb2.ImageAnalyticResponse()
    img_anly_resp.face_enrollments.extend([fc_enrl_res])
    return [img_anly_resp]


@pytest.fixture
def labels():
    return {"test": "success"}


@pytest.fixture
def request_object_with_labels(image_representation,
                               type_of_analytic_requested, labels):
    """example of case with labels"""
    return [
        ImageAnalyticRequest(image_representation,
                             type_of_analytic_requested,
                             labels=labels)
    ]


class TestImageAnalyticClient:
    """Test Image Analytic Client to works well"""
    def test_initiate_client_object_with_default_transport(self):
        """Test init function on image analytic client works with create default transport"""
        os.environ[env.NODEFLUX_ACCESS_KEY] = "APIKEYFROMENV"
        os.environ[env.NODEFLUX_SECRET_KEY] = "SECRETKEYFROMENV"

        clients = ImageAnalyticClient()

        assert isinstance(clients.transport, ImageAnalyticGrpcTransport)

    def test_initiate_client_object_with_given_transport(self):
        """Test init function on image analytic client works with create custom transport"""
        os.environ[env.NODEFLUX_ACCESS_KEY] = "APIKEYFROMENV"
        os.environ[env.NODEFLUX_SECRET_KEY] = "SECRETKEYFROMENV"
        transports = ImageAnalyticGrpcTransport()

        clients = ImageAnalyticClient(transport=transports)

        assert isinstance(clients.transport, ImageAnalyticGrpcTransport)

    def test_batch_image_analytic_request_success_to_run(
            self, request_object, response_object, mocker):
        """Test batch_image_analytic will works well and return right response"""
        mocker.patch.object(ImageAnalyticGrpcTransport,
                            'stream_image_analytic',
                            return_value=response_object,
                            autospec=True)

        client = ImageAnalyticClient()

        response = client.batch_image_analytic(request_object)

        assert isinstance(response,
                          image_analytic_pb2.BatchImageAnalyticResponse)
        for response in response.responses:
            if response.face_enrollments:
                for enrolled in response.face_enrollments:
                    assert enrolled.face_id == 12333923

    def test_stream_image_analytic_request_success_to_run(
            self, request_object, response_object, mocker):
        """Test stream_image_analytic will works well and return right response"""
        mocker.patch.object(ImageAnalyticGrpcTransport,
                            'stream_image_analytic',
                            return_value=response_object,
                            autospec=True)

        client = ImageAnalyticClient()

        response = client.stream_image_analytic(request_object)

        for item in response:
            assert isinstance(item, image_analytic_pb2.ImageAnalyticResponse)
            if item.face_enrollments:
                for enrolled in item.face_enrollments:
                    assert enrolled.face_id == 12333923

    def test_itterator_creator_with_no_labels(self, request_object,
                                              image_representation,
                                              type_of_analytic_requested,
                                              mocker):
        """Test case for image analytics grpc transport to work well when request of stream image analytic comes"""
        iterator_result = nodeflux.cloud.clients.image_analytic_client._image_analytic_request_iterator(
            request_object)

        assert isinstance(iterator_result, types.GeneratorType)
        for item in iterator_result:
            assert isinstance(item, image_analytic_pb2.ImageAnalyticRequest)
            assert item.image.content == image_representation
            if item.analytics:
                for i in range(len(type_of_analytic_requested)):
                    assert item.analytics[i] == analytic_pb2.Analytic(
                        type=type_of_analytic_requested[i])

    def test_itterator_creator_with_labels(self, request_object_with_labels,
                                           mocker, image_representation,
                                           type_of_analytic_requested):
        """Test case for image analytics grpc transport to work well when request of stream image analytic comes"""
        iterator_result = nodeflux.cloud.clients.image_analytic_client._image_analytic_request_iterator(
            request_object_with_labels)

        assert isinstance(iterator_result, types.GeneratorType)
        for item in iterator_result:
            assert isinstance(item, image_analytic_pb2.ImageAnalyticRequest)
            assert item.image.content == image_representation
            if item.analytics:
                for i in range(len(type_of_analytic_requested)):
                    assert item.analytics[i] == analytic_pb2.Analytic(
                        type=type_of_analytic_requested[i])
            assert item.metadata.labels["test"] == "success"
